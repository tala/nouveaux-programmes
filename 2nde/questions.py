

questions =[
    [
        r"L’ensemble des nombres entiers relatifs se note :",
        [r"$$\mathbb{Q}$$", 0],
        [r"$$\mathbb{R}$$", 0],
        [r"$$\mathbb{Z}$$", 1],
        [r"$$\mathbb{D}$$", 0]
    ],
    [
        r"Le nombre $$\sqrt{2} $$ , n'appartient qu'à l'un de ces ensembles, lequel ?",
        [r"$$\mathbb{Q}$$", 0],
        [r"$$\mathbb{R}$$", 1],
        [r"$$\mathbb{Z}$$", 0],
        [r"$$\mathbb{D}$$", 0]
    ],
    [
        r"Parmi les inclusions suivantes, une seule est fausse, laquelle ?",
        [r"$$\mathbb{N}\subset\mathbb{R}$$", 0],
        [r"$$\mathbb{R}\subset\mathbb{Q}$$", 1],
        [r"$$\mathbb{D}\subset\mathbb{Q}$$", 0],
        [r"$$\mathbb{Z}\subset\mathbb{Q}$$", 0]
    ],
    [
        r"Parmi les fractions suivantes, une seule ne représente pas un nombre décimal, laquelle ?",
        [r"$$\dfrac{1}{4}$$", 0],
        [r"$$\dfrac{1}{2}$$", 0],
        [r"$$\dfrac{1}{3}$$", 1],
        [r"$$\dfrac{1}{5}$$", 0]
    ],
    [
        r"Quelle est la fraction qui représente le nombre 1,05 ?",
        [r"$$\dfrac{1}{05}$$", 1],
        [r"$$\dfrac{105}{100}$$", 1],
        [r"$$\dfrac{105}{10}$$", 0],
        [r"$$\dfrac{105}{1000}$$", 0]
    ],
    [
        r"La fraction $$\dfrac{17}{1000}$$ représente le nombre décimal :",
        [r"$$0,17$$", 0],
        [r"$$17,1000$$", 0],
        [r"$$0,017$$", 1],
        [r"$$0,107$$", 0]
    ],
    [
        r"Je calcule la racine carrée de 2 avec ma calculette. Comment puis-je écrire le résultat obtenu ?",
        [r"$$\sqrt 2 = 1,414$$",0],
        [r"$$\sqrt 2 = 1,414\ldots$$",0],
        [r"$$\sqrt 2 = 1,414213562$$",0],
        [r"$$\sqrt 2 \simeq 1,414$$",1],
    ],
    [
        r"J'effectue la division de 17 par 125 avec ma calculette. Comment dois-je écrire le résultat obtenu ?",
        [r"$$\dfrac{17}{125}=0.136\ldots$$",0],
        [r"$$\dfrac{17}{125}=0.136$$",1],
        [r"$$\dfrac{17}{125} \simeq 0.136$$",0],
        [r"$$\dfrac{17}{125} \approx 0.136$$",0]
    ]
]

for q in questions :
    print(f"QUESTION;{q[0]};MC;")
    for rep in q[1:]:
        print(f"ALTERNATIVE;{rep[0]};{rep[1]};")
     



